import sys
import re
import json

# READING
with open(sys.argv[1], 'r') as input_file:
    program = input_file.read().strip().split('\n')

# Tokenizer
i = 0
op_set = set(['MOV', 'LOAD', 'STORE', 'ADD', 'MUL', 'SUB', 'XOR', 'OR', 'AND', 'NOP', 'CMP', 'BEQ', 'BNEQ', 'GTE', 'BL'])
registers = {}
labels = {}
while i < len(program):
    program[i] = re.split('\s|,', program[i])
    program[i] = list(filter(lambda x: x, program[i]))
    if not program[i]:
        program.pop(i)
    if program[i][0] in op_set:
        program[i] = [''] + program[i]
    for index, word in enumerate(program[i]):
        if index == 0:
            program[i][index] = {'token': 'label', 'val': word if word else None}
        if index == 1:
            program[i][index] = {'token': 'op', 'val': word}
        if index >= 2 and index <= 4 and re.match('R\d+', word):
            program[i][index] = {'token': 'register', 'val': word}
            registers[word] = None
        if re.match('(#|0x|0b)\d+(<\d+|>\d+)?', word):
            program[i][index] = {'token': 'rval', 'val': word}
        if index == 0 and re.match('[A-Za-z_][A-Za-z_0-9]*', word):
            labels[word] = i
    i += 1

# DECODING
# Treba mapirati sve instrukcije na funkcije iz posebnog modula: MOV, LOAD, STORE, DW, SUB, MUL...

[print(x) for x in program]
print(json.dumps(registers, indent=2))
print(json.dumps(labels, indent=2))
